<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {

    return [
        'text' => $faker->text,
        'title' => $faker->title,
        'lecturer' => $faker->name,
        'sessions' => $faker->numberBetween(1, 200)
    ];

});
