<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('/auth')->group(function (){
    Route::post('/login', 'Api\UserController@login');
    Route::post('/register', 'Api\AuthController@register');
});

Route::prefix('/courses')->group(function() {

    Route::post('/create', 'Api\CourseController@create');
    Route::post('/register', 'Api\CourseController@register')->middleware('user.jwt.auth');
    Route::post('/list', 'Api\CourseController@courses')->middleware('user.jwt.auth');
    Route::get('/export', 'Api\CourseController@export');
});
