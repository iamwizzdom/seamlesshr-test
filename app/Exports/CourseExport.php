<?php

namespace App\Exports;

use App\Course;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class CourseExport implements FromQuery
{
    use Exportable;
    /**
     * @return Builder
     */
    public function query()
    {
        return Course::query();
    }
}
