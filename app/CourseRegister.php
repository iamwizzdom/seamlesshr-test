<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseRegister extends Model
{
    protected $table = 'course_register';

    protected $fillable = ['course_id', 'user_id'];
}
