<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    public function login(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8'
        ]);


        if ($validator->fails()) {
            return response([
                'status' => false,
                'code' => 400,
                'title' => 'Login failed',
                'message' => $validator->errors()
            ], 400);
        }

        $credentials = $request->only(['email', 'password']);

        try {

            if (!$token = JWTAuth::attempt($credentials)) {
                return response([
                    'status' => false,
                    'code' => 401,
                    'title' => 'Login failed',
                    'message' => "Invalid credentials"
                ], 401);
            }

        } catch (JWTException $exception) {
            return response([
                'status' => false,
                'code' => 500,
                'title' => 'Login failed',
                'message' => "An authentication token could not be generated at this time"
            ], 500);
        }

        return response([
            'status' => true,
            'code' => 200,
            'title' => 'Login successful',
            'message' => 'You have been logged in successfully.',
            'response' => [
                'token' => $token
            ]
        ], 200);
    }

    public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:8'
        ]);

        if ($validator->fails()) {
            return response([
                'status' => false,
                'code' => 400,
                'title' => 'Registration failed',
                'message' => $validator->errors()
            ], 400);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        if (!$user) {
            return response([
                'status' => false,
                'title' => 'Registration failed',
                'message' => 'Sorry, we could not register you at this time.'
            ], 417);
        }

        $user->offsetUnset('id');

        return response([
            'status' => true,
            'code' => 200,
            'title' => 'Registration successful',
            'message' => 'You have been registered successfully.',
            'response' => [
                'user' => $user,
                'token' => JWTAuth::fromUser($user)
            ]
        ], 200);

    }
}
