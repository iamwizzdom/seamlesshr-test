<?php

namespace App\Http\Controllers\Api;

use App\CourseRegister;
use App\Exports\CourseExport;
use App\Jobs\Course;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Excel;

class CourseController extends Controller
{

    public function create() {

        Course::dispatch(50)->onConnection('database')->onQueue('courses');

        return response([
            'status' => true,
            'code' => 200,
            'title' => 'Course successful',
            'message' => 'Courses are now being created and will be available soon'
        ]);
    }

    public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            'courses' => 'required|array'
        ]);

        if ($validator->fails()) {
            return response([
                'status' => false,
                'code' => 400,
                'title' => 'Course registration failed',
                'message' => $validator->errors()
            ], 400);
        }

        $registrationCount = 0;

        foreach ($request->courses as $id) {

            $course = \App\Course::where('id', $id)->first();

            if (!$course) continue;

            if (Auth::user()->isRegisteredForCourse($id)) continue;

            if (CourseRegister::create([
                'course_id' => $id,
                'user_id' => Auth::user()->id,
            ])) $registrationCount++;
        }

        return response([
            'status' => true,
            'code' => 200,
            'title' => 'Course registration successful',
            'message' => "{$registrationCount} course(s) registered successfully"
        ]);

    }

    public function courses() {
        $courses = \App\Course::all();

        foreach ($courses as &$course) {
            if ($register = Auth::user()->isRegisteredForCourse($course->id)) {
                $course->offsetSet('date_enrolled', $register->created_at);
            }
        }

        return response()->json([
            'status' => true,
            'code' => 200,
            'title' => 'List of courses',
            'message' => "List retrieved successfully",
            'response' => [
                'courses' => $courses
            ]
        ]);
    }

    public function export() {
        return (new CourseExport())->download('courses.xlsx', Excel::XLSX);
    }
}
